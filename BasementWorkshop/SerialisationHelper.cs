﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BasementWorkshop
{
    static class SerialisationHelper
    {
        /// <summary>
        /// Applies values from a JSON dictionary onto an object whose class contains properties with the JsonKey attribute applied.
        /// </summary>
        /// <typeparam name="T">Type of the target object</typeparam>
        /// <param name="json">JSON to be applied to the object</param>
        /// <param name="obj">The object to which the values should be applied to</param>
        public static void SetProperiesFromJson<T>(string json, T obj)
        {
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            var type = typeof(T);
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo property in properties)
            {
                if (property.CanWrite)
                {
                    var keyAttribute = property.GetCustomAttributes<JsonKeyAttribute>().FirstOrDefault();
                    if (keyAttribute != null)
                    {
                        string key = keyAttribute.Key;
                        if (dictionary.ContainsKey(key))
                        {
                            property.SetValue(obj, dictionary[key]);
                        }
                    }
                }
            }
        }
    }
}
