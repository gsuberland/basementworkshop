﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasementWorkshop
{
    public partial class MainForm : Form
    {
        private bool _updatingUI = false;
        private bool _stateLoaded = false;

        private BasementSettings _settings = null;
        private GameState _gameState = null;

        private BindingList<District> _districts = new BindingList<District>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void DisableControls()
        {
            // update all controls apart from menus
            foreach (var control in this.Controls)
            {
                if (control is ToolStripMenuItem)
                    continue;
                if (control is MenuStrip)
                    continue;

                ((Control)control).Enabled = false;
            }
        }

        private void EnableControls()
        {
            // update all controls apart from menus
            foreach (var control in this.Controls)
            {
                if (control is ToolStripMenuItem)
                    continue;
                if (control is MenuStrip)
                    continue;

                ((Control)control).Enabled = true;
            }
        }

        public void UpdateTitle()
        {
            this.Text += " v" + Application.ProductVersion;
        }

        public void FillDefaultData()
        {
            cmbDistrictAuthority.Items.Clear();
            var authorities = Enum.GetValues(typeof(DistrictAuthority));
            foreach (DistrictAuthority authority in authorities)
            {
                cmbDistrictAuthority.Items.Add(authority);
            }
        }

        /// <summary>
        /// Updates the UI controls from the currently loaded game state.
        /// </summary>
        public void UpdateControlsFromGameState()
        {
            if (!_stateLoaded)
                throw new BasementWorkshopAssertException("MainForm.UpdateControlsFromGameState called when game state not loaded.");
            if (_gameState == null)
                throw new BasementWorkshopAssertException("MainForm.UpdateControlsFromGameState called when game state is null.");


            txtMoney.Text = _gameState.Profile.money.ToString();

            _districts.Clear();
            foreach (var district in _gameState.City.districts)
            {
                _districts.Add(district);
            }
            cmbDistricts.DataSource = _districts;
            cmbDistricts.Refresh();
        }

        /// <summary>
        /// Fixes a sizing issue with the rename district button.
        /// </summary>
        private void FixRenameDistrictButton()
        {
            // this button appears correctly sized in the form designer, but ends up too "short" vertically in practice
            // not sure why this occurs, but I'm not gonna lose any sleep over it.
            btnRenameDistrict.Top = cmbDistricts.Top;
            btnRenameDistrict.Height = cmbDistricts.Height;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UpdateTitle();
            FixRenameDistrictButton();
            DisableControls();
            FillDefaultData();
            _settings = new BasementSettings();
        }

#region " Interaction Events "

        private void gameLoadMenu_Click(object sender, EventArgs e)
        {
            // load the game state from registry
            _gameState = GameState.LoadGame(_settings);
            _stateLoaded = true;

            // update the UI
            _updatingUI = true;
            UpdateControlsFromGameState();
            EnableControls();
            _updatingUI = false;

            // switch districts after updatingUI has been cleared to allow it to sync everything up
            cmbDistricts.SelectedIndex = 0;
        }
        private void gameSaveMenu_Click(object sender, EventArgs e)
        {
            GameState.SaveGame(_settings, _gameState);
        }

        private void cmbDistricts_SelectedIndexChanged(object sender, EventArgs e)
        {
            // selected district changed

            if (cmbDistricts.SelectedIndex >= 0)
            {
                _updatingUI = true;
                // get currently selected district
                var district = cmbDistricts.SelectedItem as District;
                if (district != null)
                {
                    // update the UI to reflect the newly selected district
                    cmbDistrictAuthority.SelectedItem = district.authority;
                    nudJunkies.Value = district.startJunkieCount;
                    nudPopulation.Value = district.startPopulation;
                }
                else
                {
                    Logger.Warn("MainForm.cmbDistricts_SelectedIndexChanged", "Selected district was null");
                }
                _updatingUI = false;
            }
        }
        private void cmbDistrictAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            // authority for the district has changed, so update game state with new value

            if (!_updatingUI)
            {
                var district = cmbDistricts.SelectedItem as District;
                if (district != null)
                {
                    // update the authority
                    district.authority = (DistrictAuthority)cmbDistrictAuthority.SelectedItem;
                }
                else
                {
                    Logger.Warn("MainForm.cmbDistrictAuthority_SelectedIndexChanged", "Selected district was null");
                }
            }
        }

        private void nudJunkies_ValueChanged(object sender, EventArgs e)
        {
            // junkie counter for the district has changed, so update game state with new value

            if (!_updatingUI)
            {
                var district = cmbDistricts.SelectedItem as District;
                if (district != null)
                {
                    district.startJunkieCount = (int)nudJunkies.Value;
                }
                else
                {
                    Logger.Warn("MainForm.nudJunkies_ValueChanged", "Selected district was null");
                }
            }
        }

        private void nudPopulation_ValueChanged(object sender, EventArgs e)
        {
            // population counter for the district has changed, so update game state with new value

            if (!_updatingUI)
            {
                var district = cmbDistricts.SelectedItem as District;
                if (district != null)
                {
                    district.startPopulation = (int)nudPopulation.Value;
                }
                else
                {
                    Logger.Warn("MainForm.nudPopulation_ValueChanged", "Selected district was null");
                }
            }
        }

        private void txtMoney_Validated(object sender, EventArgs e)
        {
            // money value has been changed and validated, so update the game state with the new money amount

            if (!_updatingUI)
            {
                long money = 0;
                if (long.TryParse(txtMoney.Text, out money))
                {
                    _gameState.Profile.money = money;
                }
                else
                {
                    Logger.Warn("MainForm.txtMoney_Validated", "Failed to parse money amount");
                }
            }
        }

        private void btnRenameDistrict_Click(object sender, EventArgs e)
        {
            var district = cmbDistricts.SelectedItem as District;
            if (district != null)
            {
                string newDistrictName = null;
                while (true)
                {
                    newDistrictName = InputBoxForm.Input("Rename district", "District name:", "Rename");

                    if (newDistrictName == null)
                    {
                        // they clicked cancel
                        break;
                    }
                    else if (string.IsNullOrWhiteSpace(newDistrictName))
                    {
                        // it's not null because of the above clause, so it's empty or whitespace (bad name)
                        MessageBox.Show("Please enter a valid district name.", "Invalid name");
                    }
                    else
                    {
                        // name checks out - do the rename
                        district.name = newDistrictName;
                        // force the bound district object to be updated so that the UI refreshes
                        _districts.ResetItem(cmbDistricts.SelectedIndex);
                        break;
                    }
                }
            }
            else
            {
                Logger.Warn("MainForm.btnRenameDistrict_Click", "Selected district was null");
            }
        }

#endregion

#region " Validation Events "

        private void txtMoney_Validating(object sender, CancelEventArgs e)
        {
            Int64 moneyValue = 0;
            bool success = Int64.TryParse(txtMoney.Text, out moneyValue);
            if (!success)
            {
                e.Cancel = true;
                // todo: find a more sensible / generic way to show this error message
                MessageBox.Show("Money value must be numeric and within range.", "Validation error");
            }
        }

#endregion
    }
}
