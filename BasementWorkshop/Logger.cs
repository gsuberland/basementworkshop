﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasementWorkshop
{
    enum LogLevel
    {
        Info,
        Warning,
        Error
    }

    struct LogEntry
    {
        public LogLevel Level;
        public string Source;
        public string Message;

        public LogEntry(LogLevel level, string source, string message)
        {
            this.Level = level;
            this.Source = source;
            this.Message = message;
        }
    }

    class Logger
    {
        private static object _syncRoot = new object();
        private static List<LogEntry> _entries = new List<LogEntry>();

        public static void Log(LogLevel level, string source, string message)
        {
            var entry = new LogEntry(level, source, message);
            lock(_syncRoot)
            {
                _entries.Add(entry);
            }
        }

        public static void Info(string source, string message)
        {
            Log(LogLevel.Info, source, message);
        }

        public static void Warn(string source, string message)
        {
            Log(LogLevel.Warning, source, message);
        }

        public static void Error(string source, string message)
        {
            Log(LogLevel.Error, source, message);
        }
    }
}
