﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BasementWorkshop
{
    class CitySaveData
    {
        public int mainDistrictIndex;
        public List<Truck> trucks;
        public List<District> districts;
    }
}
