﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace BasementWorkshop
{
    /// <summary>
    /// Class for interfacing with Basement's settings, including game state, in the registry.
    /// </summary>
    /// <remarks>
    /// Note: This type's methods should be thread-safe.
    /// </remarks>
    class BasementSettings : IDisposable
    {
        /// <summary>Synchronisation object</summary>
        private readonly object _syncRoot;
        /// <summary>Registry key instance for Basement's settings</summary>
        private readonly RegistryKey _basementKey;

        /// <summary>Path to the Basement registry key</summary>
        private const string BasementRegistryKey = @"Software\Halfbus\Basement";

        public BasementSettings()
        {
            this._syncRoot = new object();
            // open a handle to Basement's registry key in writeable mode
            lock (_syncRoot)
            {
                this._basementKey = Registry.CurrentUser.OpenSubKey(BasementRegistryKey, true);
            }
        }

        public void Dispose()
        {
            lock (_syncRoot)
            {
                // free the handle to Basement's registry key
                if (this._basementKey != null)
                    this._basementKey.Dispose();
            }
        }

        /// <summary>
        /// Finds the registry value name that corresponds to a Basement setting name.
        /// </summary>
        /// <param name="name">The name of the setting to find the value name for.</param>
        /// <returns>The name of the registry value name, or null if no value name could be identified.</returns>
        public string FindValueNameBySettingName(string name)
        {
            string decoratedName = name + "_h";
            string[] valueNames = null;
            lock (_syncRoot)
            {
                valueNames = _basementKey.GetValueNames();
            }
            return valueNames.FirstOrDefault(vn => vn.StartsWith(decoratedName));
        }

        /// <summary>
        /// Reads a setting from Basement's registry key.
        /// </summary>
        /// <param name="name">The name of the setting to read.</param>
        /// <returns>A string representing the setting value.</returns>
        /// <exception cref="CouldNotLoadGameException"/>
        public string ReadSetting(string name)
        {
            // attempt to resolve the setting name to its associated registry value name
            string valueName = FindValueNameBySettingName(name);
            if (valueName == null)
                throw new CouldNotLoadGameStateException(string.Format("Setting name '{0}' could not be resolved.", name ?? "<null>"));

            // attempt to read the value data
            object settingData = null;
            lock (_syncRoot)
            {
                settingData = _basementKey.GetValue(valueName, null);
            }
            if (settingData == null || !(settingData is byte[]))
                throw new CouldNotLoadGameStateException(string.Format("Setting '{0}' could not be read.", name ?? "<null>"));

            // we now have data; parse it as an ASCII string, trimming the trailing null byte
            string dataString = Encoding.ASCII.GetString((byte[])settingData).TrimEnd('\0');

            return dataString;
        }

        /// <summary>
        /// Saves a setting value to Basement's registry key.
        /// </summary>
        /// <param name="name">The name of the setting to write to.</param>
        /// <param name="value">The value to save.</param>
        /// <exception cref="CouldNotSaveGameException"/>
        public void SaveSetting(string name, string value)
        {
            // attempt to resolve the setting name to its associated registry value name
            string valueName = FindValueNameBySettingName(name);
            if (valueName == null)
                throw new CouldNotSaveGameStateException(string.Format("Setting name '{0}' could not be resolved.", name ?? "<null>"));

            // translate setting value to ASCII with null terminator
            byte[] settingData = Encoding.ASCII.GetBytes(value).Concat(new byte[] { 0 }).ToArray();

            // attempt to save the value data
            lock (_syncRoot)
            {
                _basementKey.SetValue(valueName, settingData, RegistryValueKind.Binary);
            }
        }
    }
}
