﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasementWorkshop
{
    public enum CharacterType
    {
        Worker,
        Security,
        Dealer,
        Cop,
        Citizen,
        Junkie
    }

    class CharacterData
    {
        public string characterSkin;
        public int nameID;
        public int biographyID;
        public string avatarID;
        public float speed;
        public float hitCooldown;
        public float blockChance;
        public CharacterType characterType;
        protected float healthMin;
        protected float healthMax;
        protected float damageMin;
        protected float damageMax;
        public bool healthBacked;
        public float healthBacking;
        public District district;
        public District origin;
        public float initialHP;
        public float initialDMG;
    }
}
