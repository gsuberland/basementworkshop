﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasementWorkshop
{
    class BasementLayoutSettings
    {
        public int depth;
        public int horizontalSize;
        public int entranceCellId;
        public string[] defaultLayout;
        public string[] defaultCharacters;
        public string startSetting;
        public string startCharacters;
    }
}
