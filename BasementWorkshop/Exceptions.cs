﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasementWorkshop
{
    abstract class BasementWorkshopExceptionBase : Exception
    {
        public BasementWorkshopExceptionBase(string message)
            : base(message)
        { }

        public BasementWorkshopExceptionBase(string message, Exception innerException)
            : base(message, innerException)
        { }
    }

    class BasementWorkshopAssertException : BasementWorkshopExceptionBase
    {
        public BasementWorkshopAssertException(string message)
            : base(message)
        { }
    }

    /// <summary>
    /// An exception that indicates that the game state could not be loaded.
    /// </summary>
    class CouldNotLoadGameStateException : BasementWorkshopExceptionBase
    {
        public CouldNotLoadGameStateException(string message)
            : base(message)
        { }

        public CouldNotLoadGameStateException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }

    /// <summary>
    /// An exception that indicates that the game state could not be saved.
    /// </summary>
    class CouldNotSaveGameStateException : BasementWorkshopExceptionBase
    {
        public CouldNotSaveGameStateException(string message)
            : base(message)
        { }

        public CouldNotSaveGameStateException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
