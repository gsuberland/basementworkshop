﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasementWorkshop
{
    public enum DistrictAuthority
    {
        Player,
        Police,
        Neutral
    }

    class District
    {
        public string name;
        public int cityIndex;
        public float spreadProgress;
        public bool isMutable;
        public DistrictAuthority authority;
        public BasementLayoutSettings basementLayout;
        public string savedSetting;
        public string savedCharacters;
        public float dropChance;
        public int startJunkieCount;
        public int startPopulation;

        public override string ToString()
        {
            return string.Format("{0:x2}: {1}", cityIndex, name ?? "<null>");
        }
    }
}
