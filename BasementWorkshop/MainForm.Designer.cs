﻿namespace BasementWorkshop
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.gameMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.gameLoadMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.gameSaveMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.gameExportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.gameImportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpAboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.grpFinances = new System.Windows.Forms.GroupBox();
            this.txtMoney = new System.Windows.Forms.TextBox();
            this.lblMoney = new System.Windows.Forms.Label();
            this.grpDistricts = new System.Windows.Forms.GroupBox();
            this.cmbDistricts = new System.Windows.Forms.ComboBox();
            this.pnlDistrictEditor = new System.Windows.Forms.Panel();
            this.cmbDistrictAuthority = new System.Windows.Forms.ComboBox();
            this.lblDistrictAuthority = new System.Windows.Forms.Label();
            this.nudJunkies = new System.Windows.Forms.NumericUpDown();
            this.lblJunkies = new System.Windows.Forms.Label();
            this.lblPopulation = new System.Windows.Forms.Label();
            this.nudPopulation = new System.Windows.Forms.NumericUpDown();
            this.btnRenameDistrict = new System.Windows.Forms.Button();
            this.MainMenu.SuspendLayout();
            this.grpFinances.SuspendLayout();
            this.grpDistricts.SuspendLayout();
            this.pnlDistrictEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudJunkies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPopulation)).BeginInit();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameMenu,
            this.helpMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(1002, 33);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "menuStrip1";
            // 
            // gameMenu
            // 
            this.gameMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameLoadMenu,
            this.gameSaveMenu,
            this.gameExportMenu,
            this.gameImportMenu});
            this.gameMenu.Name = "gameMenu";
            this.gameMenu.Size = new System.Drawing.Size(70, 29);
            this.gameMenu.Text = "&Game";
            // 
            // gameLoadMenu
            // 
            this.gameLoadMenu.Name = "gameLoadMenu";
            this.gameLoadMenu.Size = new System.Drawing.Size(198, 30);
            this.gameLoadMenu.Text = "&Load";
            this.gameLoadMenu.Click += new System.EventHandler(this.gameLoadMenu_Click);
            // 
            // gameSaveMenu
            // 
            this.gameSaveMenu.Name = "gameSaveMenu";
            this.gameSaveMenu.Size = new System.Drawing.Size(198, 30);
            this.gameSaveMenu.Text = "&Save";
            this.gameSaveMenu.Click += new System.EventHandler(this.gameSaveMenu_Click);
            // 
            // gameExportMenu
            // 
            this.gameExportMenu.Enabled = false;
            this.gameExportMenu.Name = "gameExportMenu";
            this.gameExportMenu.Size = new System.Drawing.Size(198, 30);
            this.gameExportMenu.Text = "E&xport...";
            // 
            // gameImportMenu
            // 
            this.gameImportMenu.Enabled = false;
            this.gameImportMenu.Name = "gameImportMenu";
            this.gameImportMenu.Size = new System.Drawing.Size(198, 30);
            this.gameImportMenu.Text = "I&mport...";
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpAboutMenu});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(61, 29);
            this.helpMenu.Text = "&Help";
            // 
            // helpAboutMenu
            // 
            this.helpAboutMenu.Name = "helpAboutMenu";
            this.helpAboutMenu.Size = new System.Drawing.Size(134, 30);
            this.helpAboutMenu.Text = "&About";
            // 
            // grpFinances
            // 
            this.grpFinances.Controls.Add(this.txtMoney);
            this.grpFinances.Controls.Add(this.lblMoney);
            this.grpFinances.Location = new System.Drawing.Point(12, 49);
            this.grpFinances.Name = "grpFinances";
            this.grpFinances.Size = new System.Drawing.Size(229, 74);
            this.grpFinances.TabIndex = 1;
            this.grpFinances.TabStop = false;
            this.grpFinances.Text = "Finances";
            // 
            // txtMoney
            // 
            this.txtMoney.Location = new System.Drawing.Point(72, 32);
            this.txtMoney.Name = "txtMoney";
            this.txtMoney.Size = new System.Drawing.Size(151, 26);
            this.txtMoney.TabIndex = 4;
            this.txtMoney.Text = "2500";
            this.txtMoney.Validating += new System.ComponentModel.CancelEventHandler(this.txtMoney_Validating);
            this.txtMoney.Validated += new System.EventHandler(this.txtMoney_Validated);
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.Location = new System.Drawing.Point(6, 35);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(60, 20);
            this.lblMoney.TabIndex = 3;
            this.lblMoney.Text = "Money:";
            // 
            // grpDistricts
            // 
            this.grpDistricts.Controls.Add(this.btnRenameDistrict);
            this.grpDistricts.Controls.Add(this.pnlDistrictEditor);
            this.grpDistricts.Controls.Add(this.cmbDistricts);
            this.grpDistricts.Location = new System.Drawing.Point(12, 129);
            this.grpDistricts.Name = "grpDistricts";
            this.grpDistricts.Size = new System.Drawing.Size(978, 571);
            this.grpDistricts.TabIndex = 2;
            this.grpDistricts.TabStop = false;
            this.grpDistricts.Text = "Districts";
            // 
            // cmbDistricts
            // 
            this.cmbDistricts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistricts.FormattingEnabled = true;
            this.cmbDistricts.Location = new System.Drawing.Point(10, 25);
            this.cmbDistricts.Name = "cmbDistricts";
            this.cmbDistricts.Size = new System.Drawing.Size(213, 28);
            this.cmbDistricts.TabIndex = 0;
            this.cmbDistricts.SelectedIndexChanged += new System.EventHandler(this.cmbDistricts_SelectedIndexChanged);
            // 
            // pnlDistrictEditor
            // 
            this.pnlDistrictEditor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDistrictEditor.Controls.Add(this.nudPopulation);
            this.pnlDistrictEditor.Controls.Add(this.lblPopulation);
            this.pnlDistrictEditor.Controls.Add(this.lblJunkies);
            this.pnlDistrictEditor.Controls.Add(this.nudJunkies);
            this.pnlDistrictEditor.Controls.Add(this.lblDistrictAuthority);
            this.pnlDistrictEditor.Controls.Add(this.cmbDistrictAuthority);
            this.pnlDistrictEditor.Location = new System.Drawing.Point(10, 59);
            this.pnlDistrictEditor.Margin = new System.Windows.Forms.Padding(6);
            this.pnlDistrictEditor.Name = "pnlDistrictEditor";
            this.pnlDistrictEditor.Size = new System.Drawing.Size(962, 506);
            this.pnlDistrictEditor.TabIndex = 1;
            // 
            // cmbDistrictAuthority
            // 
            this.cmbDistrictAuthority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrictAuthority.FormattingEnabled = true;
            this.cmbDistrictAuthority.Location = new System.Drawing.Point(104, 7);
            this.cmbDistrictAuthority.Name = "cmbDistrictAuthority";
            this.cmbDistrictAuthority.Size = new System.Drawing.Size(213, 28);
            this.cmbDistrictAuthority.TabIndex = 1;
            this.cmbDistrictAuthority.SelectedIndexChanged += new System.EventHandler(this.cmbDistrictAuthority_SelectedIndexChanged);
            // 
            // lblDistrictAuthority
            // 
            this.lblDistrictAuthority.AutoSize = true;
            this.lblDistrictAuthority.Location = new System.Drawing.Point(10, 10);
            this.lblDistrictAuthority.Name = "lblDistrictAuthority";
            this.lblDistrictAuthority.Size = new System.Drawing.Size(76, 20);
            this.lblDistrictAuthority.TabIndex = 2;
            this.lblDistrictAuthority.Text = "Authority:";
            // 
            // nudJunkies
            // 
            this.nudJunkies.Location = new System.Drawing.Point(104, 41);
            this.nudJunkies.Name = "nudJunkies";
            this.nudJunkies.Size = new System.Drawing.Size(120, 26);
            this.nudJunkies.TabIndex = 3;
            this.nudJunkies.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudJunkies.ValueChanged += new System.EventHandler(this.nudJunkies_ValueChanged);
            // 
            // lblJunkies
            // 
            this.lblJunkies.AutoSize = true;
            this.lblJunkies.Location = new System.Drawing.Point(10, 43);
            this.lblJunkies.Name = "lblJunkies";
            this.lblJunkies.Size = new System.Drawing.Size(67, 20);
            this.lblJunkies.TabIndex = 4;
            this.lblJunkies.Text = "Junkies:";
            // 
            // lblPopulation
            // 
            this.lblPopulation.AutoSize = true;
            this.lblPopulation.Location = new System.Drawing.Point(10, 75);
            this.lblPopulation.Name = "lblPopulation";
            this.lblPopulation.Size = new System.Drawing.Size(88, 20);
            this.lblPopulation.TabIndex = 5;
            this.lblPopulation.Text = "Population:";
            // 
            // nudPopulation
            // 
            this.nudPopulation.Location = new System.Drawing.Point(104, 73);
            this.nudPopulation.Name = "nudPopulation";
            this.nudPopulation.Size = new System.Drawing.Size(120, 26);
            this.nudPopulation.TabIndex = 6;
            this.nudPopulation.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudPopulation.ValueChanged += new System.EventHandler(this.nudPopulation_ValueChanged);
            // 
            // btnRenameDistrict
            // 
            this.btnRenameDistrict.Location = new System.Drawing.Point(229, 24);
            this.btnRenameDistrict.Name = "btnRenameDistrict";
            this.btnRenameDistrict.Size = new System.Drawing.Size(99, 29);
            this.btnRenameDistrict.TabIndex = 2;
            this.btnRenameDistrict.Text = "Rename";
            this.btnRenameDistrict.UseVisualStyleBackColor = true;
            this.btnRenameDistrict.Click += new System.EventHandler(this.btnRenameDistrict_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 712);
            this.Controls.Add(this.grpDistricts);
            this.Controls.Add(this.grpFinances);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.MainMenu;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Basement Workshop";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.grpFinances.ResumeLayout(false);
            this.grpFinances.PerformLayout();
            this.grpDistricts.ResumeLayout(false);
            this.pnlDistrictEditor.ResumeLayout(false);
            this.pnlDistrictEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudJunkies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPopulation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem gameMenu;
        private System.Windows.Forms.ToolStripMenuItem gameLoadMenu;
        private System.Windows.Forms.ToolStripMenuItem gameSaveMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem helpAboutMenu;
        private System.Windows.Forms.ToolStripMenuItem gameExportMenu;
        private System.Windows.Forms.ToolStripMenuItem gameImportMenu;
        private System.Windows.Forms.GroupBox grpFinances;
        private System.Windows.Forms.TextBox txtMoney;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.GroupBox grpDistricts;
        private System.Windows.Forms.ComboBox cmbDistricts;
        private System.Windows.Forms.Panel pnlDistrictEditor;
        private System.Windows.Forms.Label lblDistrictAuthority;
        private System.Windows.Forms.ComboBox cmbDistrictAuthority;
        private System.Windows.Forms.NumericUpDown nudPopulation;
        private System.Windows.Forms.Label lblPopulation;
        private System.Windows.Forms.Label lblJunkies;
        private System.Windows.Forms.NumericUpDown nudJunkies;
        private System.Windows.Forms.Button btnRenameDistrict;
    }
}

