﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BasementWorkshop
{
    class GameState
    {
        private const string GameAssemblyName = @"Assembly-CSharp";
        private const string EditorAssemblyName = @"BasementWorkshop";

        private const string CitySettingName = @"City";
        private const string ProfileSettingName = @"Profile";
        private const string StatisticsTrackerName = @"StatisticsTracker";
        private const string InspectionManagerName = @"InspectionManager";
        private const string TimeManagerName = @"TimeManager";

        public ProfileData Profile { get; set; }
        public CitySaveData City { get; set; }

        /// <summary>
        /// Translates assembly names in serialised JSON objects from the game assembly to the editor assembly, to avoid type reference errors.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private static string TranslateTypesFromGameToEditor(string json)
        {
            return json.Replace(GameAssemblyName, EditorAssemblyName);
        }

        /// <summary>
        /// Translates assembly names in serialised JSON objects from the editor assembly to the game assembly, to avoid type reference errors.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private static string TranslateTypesFromEditorToGame(string json)
        {
            return json.Replace(EditorAssemblyName, GameAssemblyName);
        }

        /// <summary>
        /// Load a game state from a set of settings entries.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static GameState LoadGame(BasementSettings settings)
        {
            // read the profile data
            string profileJSON = settings.ReadSetting(ProfileSettingName);
            profileJSON = TranslateTypesFromGameToEditor(profileJSON);
            var profileData = JsonConvert.DeserializeObject<ProfileData>(profileJSON);
            
            // read the city data
            string cityJSON = settings.ReadSetting(CitySettingName);
            cityJSON = TranslateTypesFromGameToEditor(cityJSON);
            var cityData = JsonConvert.DeserializeObject<CitySaveData>(cityJSON);

            // build a game state
            var state = new GameState
            {
                Profile = profileData,
                City = cityData,
            };
            
            return state;
        }

        /// <summary>
        /// Load a game state from a set of settings entries.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="state"></param>
        public static void SaveGame(BasementSettings settings, GameState state)
        {
            string profileJSON = JsonConvert.SerializeObject(state.Profile);
            profileJSON = TranslateTypesFromEditorToGame(profileJSON);
            settings.SaveSetting(ProfileSettingName, profileJSON);

            string cityJSON = JsonConvert.SerializeObject(state.City);
            cityJSON = TranslateTypesFromEditorToGame(cityJSON);
            settings.SaveSetting(CitySettingName, cityJSON);
        }
    }
}
