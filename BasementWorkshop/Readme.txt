﻿Basement Workshop - An editor for the game "Basement" by Halfbus.
Written by Graham "gsuberland" Sutherland (aka Polynomial)
Licensed under CC BY-SA 3.0 - see license.txt for more details.
https://bitbucket.org/gsuberland/basementworkshop

This tool edits your Basement save state, allowing you to tweak all sorts of aspects of the game.

THIS TOOL IS CURRENTLY IN AN ALPHA STATE - IT IS VERY MINIMAL IN NATURE AND MAY BREAK A LOT.
If you find a bug, please open an issue on the BitBucket repo.

Current features:

* Change money
* Rename districts
* Change district authority
* Change junkie and population counts

Planned features:

* Edit staff stats
* Edit enemy stats
* Clone districts
* Add new districts
* Delete districts
* Resize district buildings
* Edit rooms
* Edit trucks
* Change in-game time

Changelog:

v0.1.5 - Initial release.
v0.1.4 - Added ability to rename districts.
v0.1.3 - Added ability to change junkie and population numbers.
v0.1.2 - Very rough first alpha.
v0.1.1 - Pre-alpha stage, got most game state loading in.
v0.1.0 - Initial development.