﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasementWorkshop
{
    [AttributeUsage(AttributeTargets.Property)]
    class JsonKeyAttribute : Attribute
    {
        public string Key { get; private set; }

        public JsonKeyAttribute(string key)
        {
            this.Key = key;
        }
    }
}
