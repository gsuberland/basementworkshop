﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasementWorkshop
{
    public partial class InputBoxForm : Form
    {
        /// <summary>
        /// The value set by the user.
        /// </summary>
        public string Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Shows an input box.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="heading"></param>
        /// <param name="acceptText"></param>
        /// <param name="defaultText"></param>
        /// <returns>If the user clicked the accept button, returns the value they entered. Otherwise returns null.</returns>
        public static string Input(string title, string heading, string acceptText = "Accept", string defaultText = "")
        {
            using (var f = new InputBoxForm(title, heading, acceptText, defaultText))
            {
                if (f.ShowDialog() == DialogResult.OK)
                {
                    return f.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public InputBoxForm(string title, string heading, string acceptText = "Accept", string defaultText = "")
        {
            InitializeComponent();
            this.Text = title;
            lblHeading.Text = heading;
            btnAccept.Text = acceptText;
            txtInput.Text = defaultText;
        }

        private void InputBoxForm_Load(object sender, EventArgs e)
        {
            txtInput.Select();
        }

        private void InputBoxForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Value = this.txtInput.Text;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.Value = this.txtInput.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Value = this.txtInput.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
