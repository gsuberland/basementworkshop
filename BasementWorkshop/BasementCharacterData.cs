﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasementWorkshop
{
    class BasementCharacterData
    {
        public List<CharacterData> charData;
        public List<int> charAssignments;
    }
}
